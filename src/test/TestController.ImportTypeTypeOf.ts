import { Controller, Get, Path, Query, Route } from "tsoa";
import * as z from "../zod";
import type {TypeOf} from "../zod"

const User = z.object({
  name: z.string(),
  id: z.string(),
});
type User = TypeOf<typeof User>;

@Route("users-import-type-typeof")
export class UsersImportTypeTypeofController extends Controller {
  @Get("{userId}")
  public async getUser(
    @Path() id: string,
    @Query() name: string
  ): Promise<User> {
    return { id, name };
  }
}
