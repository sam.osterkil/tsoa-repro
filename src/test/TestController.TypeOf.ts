import { Controller, Get, Path, Query, Route } from "tsoa";
import * as z from "../zod";

const User = z.object({
  name: z.string(),
  id: z.string(),
});
type User = z.TypeOf<typeof User>;

@Route("users-typeof")
export class UsersTypeOfController extends Controller {
  @Get("{userId}")
  public async getUser(
    @Path() id: string,
    @Query() name: string
  ): Promise<User> {
    return { id, name };
  }
}
