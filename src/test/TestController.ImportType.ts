import { Controller, Get, Path, Query, Route } from "tsoa";
import * as z from "../zod";
import type {infer as zinfer} from "../zod"

const User = z.object({
  name: z.string(),
  id: z.string(),
});
type User = zinfer<typeof User>;

@Route("users-import-type")
export class UsersImportTypeController extends Controller {
  @Get("{userId}")
  public async getUser(
    @Path() id: string,
    @Query() name: string
  ): Promise<User> {
    return { id, name };
  }
}
