This repo exists to reproduce this issue:

```
$ yarn install
$ yarn run tsoa routes
yarn run v1.22.17
$ /home/spost/Code/tsoa-repro/node_modules/.bin/tsoa routes
There was a problem resolving type of 'User'.
Generate routes error.
 Error: No matching model found for referenced type infer.
    at new GenerateMetadataError (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/exceptions.js:22:28)
    at TypeResolver.getModelTypeDeclaration (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/typeResolver.js:715:19)
    at TypeResolver.typeArgumentsToContext (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/typeResolver.js:836:32)
    at TypeResolver.getReferenceType (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/typeResolver.js:551:14)
    at TypeResolver.resolve (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/typeResolver.js:367:34)
    at TypeResolver.getTypeAliasReference (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/typeResolver.js:594:353)
    at TypeResolver.getReferenceType (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/typeResolver.js:569:38)
    at TypeResolver.resolve (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/typeResolver.js:367:34)
    at TypeResolver.resolve (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/typeResolver.js:357:118)
    at MethodGenerator.Generate (/home/spost/Code/tsoa-repro/node_modules/@tsoa/cli/dist/metadataGeneration/methodGenerator.js:59:76)
error Command failed with exit code 1.
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.
```
